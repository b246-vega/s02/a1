#VALIDATION
while True:
    try:
        year = int(input("Enter a year: "))
        if year <= 0:
            raise ValueError("Year must be a positive integer")
        break
    except ValueError:
        print("Invalid input. Year must be a positive integer.")
#1.
if (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0):
    print(year, "is a leap year.")
else:
    print(year, "is not a leap year.")
#2.
row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()