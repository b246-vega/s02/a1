username = input("Please Enter your name : \n")

print(f"Hello {username}! welcome to the python short course")

num1 =  input("Enter 1st Number : \n")
num2 = input("Enter 2nd number : \n")

print(f"The sum of num1 and num2 is {num1+num2}")

# the output is the concatenation of num1 and num2. the reason for this is the input() method assigns any values as strings.

num1 = int(input("Enter 1st number : \n"))
num2 = int(input("Enter 2nd number : \n"))
print(f"The sum of num1 and num2 is {num1+num2}")

#-------------
#[SECTION] if-else statements
test_num = 75
if test_num >= 60:
    print("test passed")
else:
    print("test failed")

#Note that in python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else codeblock. Hence, indentions are very important as Pythob usees indentions to distinguish parts of code as needed.
#[Sections] If-else chains can be also used to have more than 2 choices for the program
test_num2 = int(input("Please enter the 2nd test number"))

if test_num2 > 2:
    print("The number is positive")
elif test_num2 == 0:
    print("The number is zero")
else:
    print("The number is negative")
    # note: elif is tthe shorthand for "else if" in other programming languages

num = int(input("Please enter the test number : \n"))

if num % 3:
    print("The number is divisible by 3")
elif num % 5:
    print("The number is divisible by 5")
elif num % 3 and num % 5:
    print("The number is both divisible by 3 and 5")
else:
    print("The number is not divisible by 3 and 5")

#[SECTION] Loops
#Python has loops that can repeat blocks of codes
#>> While loop are used to execute a set of statement as long as the condition is true 
i = 1
while i <= 5:
    print(f"Current count{i}")
    i += 1
#>> for loops are used for iterating a sequence
fruits = ["apple", "bannana", "cherry"]
for indiv_fruit in fruits: 
    print(indiv_fruit)

#[SECTION] range() method
# to use the for loop to iterate throug values, the range method can be used
for x in range(6):
    print(f"The current value is {x}")

#[SECTION] Break statement
j = 1
while j < 6:
    print(j)
    if j == 3:
        break
    j+=1
#[SECTION] continue Statement
# The continue statement returns the control to the beginning of the while loop and continue to the next iteration
z= 1
while z < 6:
    z+=1
    if z== 3:
        continue
    print(z)